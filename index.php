<?php
declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

\Dotenv\Dotenv::createImmutable(__DIR__)->load();

$boxes = ['Inbox', 'SentItems'];
$messages = [];

foreach ($boxes as $box) {
    $messages[$box] = [];
    $page = 1;
    $url = makeMessagesUrl($box);
    do {
        echo "fetching $box messages page " . $page++ . PHP_EOL;
        $response = get($url);
        foreach ($response['value'] as $message) {
            if ($message['hasAttachments']) {
                echo "fetching attachments for " . $message['id'] . PHP_EOL;
                $message['attachments'] = get(makeAttachmentUrl($message['id']))['value'];
            }
            array_push($messages[$box], $message);
        }
    } while ($url = $response['@odata.nextLink'] ?? null);
}

file_put_contents(__DIR__ . '/messages.json', json_encode($messages));

function get(string $url): array
{
    return \Zttp\Zttp::withHeaders(['Authorization' => 'Bearer ' . getenv('ACCESS_TOKEN')])->get($url)->json();
}

function makeMessagesUrl(string $box): string
{
    return 'https://graph.microsoft.com/v1.0/me/mailFolders(\'' . $box . '\')/messages?$top=100';
}

function makeAttachmentUrl(string $id): string
{
    return 'https://graph.microsoft.com/v1.0/me/messages/' . $id . '/attachments';
}
