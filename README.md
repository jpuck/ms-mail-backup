# Backup Microsoft Office 365 Outlook Email

If you don't have the premium Outlook application to backup .pst files, then you can use the [Graph REST API][api] to make a JSON dump.

The `Inbox` and `SentItems` folders with attachments are currently supported.

## Install Dependencies

```
composer install
```

[Obtain an access token from the graph explorer][token], and make sure to grant basic read mail permissions.
Save the token to the .env under the name `ACCESS_TOKEN`.

## Backup Email

depending on the size of your boxes and especially the attachments, you'll probably want to boost memory:

```
php -d memory_limit=2G index.php
```

Everything will be written to a `messages.json` file in the current working directory.

## Restoring Attachments

Image attachments are base64 encoded.
For example, decode the first Inbox message's attachment with its original filename:

```
jq -r .Inbox[0].attachments[0].contentBytes messages.json | base64 -D > "$(jq -r .Inbox[0].attachments[0].name messages.json)"
```

[api]:https://docs.microsoft.com/en-us/graph/api/resources/mail-api-overview?view=graph-rest-1.0
[token]:https://developer.microsoft.com/en-us/graph/graph-explorer
